﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Prenumeratoriaus klase
/// </summary>
public class Prenumeratorius
{
    public string Pavarde { get; set; }                                                    // Prenumeratoriaus pavarde
    public string Adresas { get; set; }                                                    // Prenumeratoriaus adresas
    public int LaikotarpioPradzia { get; set; }                                            // Prenumeratoriaus leidinio prenumeratos laikotarpio pradzia
    public int LaikotarpioIlgis { get; set; }                                              // Prenumeratoriaus leidinio prenumeratos ilgis
    public string LeidinioKodas { get; set; }                                              // Prenumeratoriaus prenumeruojamo leidinio kodas
    public int LeidiniuKiekis { get; set; }                                                // Prenumeratoriaus prenumeruojamu leidiniu kiekis

    /// <summary>
    /// Tuscias konstruktorius
    /// </summary>
    public Prenumeratorius() { }

    /// <summary>
    /// Konstruktorius nustatantis pradines prenumeratoriaus reiksmes
    /// </summary>
    /// <param name="pavarde">Prenumeratoriaus pavarde</param>
    /// <param name="adresas">Prenumeratoriaus adresas</param>
    /// <param name="laikotarpioPradzia">Prenumeratos laikotarpio pradzia</param>
    /// <param name="laikotarpioIlgis">Prenumeratos laikotarpio ilgis</param>
    /// <param name="leidinioKodas">Prenumeruojamo leidinio kodas</param>
    /// <param name="leidiniuKiekis">Prenumeruojamo leidinio kiekis</param>
    public Prenumeratorius(string pavarde, string adresas, int laikotarpioPradzia,
                            int laikotarpioIlgis, string leidinioKodas, int leidiniuKiekis)
    {
        Pavarde = pavarde;
        Adresas = adresas;
        LaikotarpioPradzia = laikotarpioPradzia;
        LaikotarpioIlgis = laikotarpioIlgis;
        LeidinioKodas = leidinioKodas;
        LeidiniuKiekis = leidiniuKiekis;
    }

    /// <summary>
    /// Funkcija skirta parengti string tipo eilute duomenu isvedimui
    /// </summary>
    /// <returns>String tipo eilute su visais prenumeratoriaus duomenimis</returns>
    public override string ToString()
    {
        return String.Format("|{0, -22}|{1, -33}|{2, 15}|{3, 8}|{4, -14}|{5, 8}|", Pavarde, Adresas, LaikotarpioPradzia,
                                                                                  LaikotarpioIlgis, LeidinioKodas, LeidiniuKiekis);
    }
}