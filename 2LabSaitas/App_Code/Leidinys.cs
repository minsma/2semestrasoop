﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Leidinio klase, kuri yra konteineris prenumeratoriu sarasui
/// </summary>
public class Leidinys
{
    public string Kodas { get; set; }                                                       // Leidinio kodas
    public string Pavadinimas { get; set; }                                                 // Leidinio pavadinimas
    public double VienoMenesioKaina { get; set; }                                           // Vieno menesio leidinio kaida
    public PrenumeratoriuSarasas PrenumSarasas;                                             // Leidini prenumeruojanciu asmenu sarasas

    /// <summary>
    /// Tuscias konstruktorius
    /// </summary>
    public Leidinys() { }

    /// <summary>
    /// Konstruktorius, kuriame nustatomi pradiniai duomenys leidiniui
    /// </summary>
    /// <param name="kodas">Leidinio kodas</param>
    /// <param name="pavadinimas">Leidinio pavadinimas</param>
    /// <param name="vienoMenesioKaina">Leidinio vieno menesio prenumeratos kaina</param>
    public Leidinys(string kodas, string pavadinimas, double vienoMenesioKaina)
    {
        Kodas = kodas;
        Pavadinimas = pavadinimas;
        VienoMenesioKaina = vienoMenesioKaina;
        PrenumSarasas = new PrenumeratoriuSarasas();
    }

    /// <summary>
    /// Funkcija skirta paruosti duomenis spausdinimui
    /// </summary>
    /// <returns>Grazinama string tipo eilute su leidinio visais duomenimis</returns>
    public override string ToString()
    {
        return String.Format("|{0, -16}|{1,-21}|{2, 19}|", Kodas, Pavadinimas, VienoMenesioKaina);
    }

    /// <summary>
    /// > operatoriaus uzklojimas skirtas rikiavimui
    /// </summary>
    /// <param name="pirmas">Pirmas Leidinio tipo objektas</param>
    /// <param name="antras">Antras leidinio tipo objektas</param>
    /// <returns>Grazinama true, jeigu pirmo pavadinimas didesnis ir atitinkamai atvirksciai</returns>
    static public bool operator >(Leidinys pirmas, Leidinys antras)
    {
        return String.Compare(pirmas.Pavadinimas, antras.Pavadinimas, StringComparison.CurrentCulture) > 0;
    }

    /// <summary>
    /// > operatoriaus uzklojimas skirtas rikiavimui
    /// </summary>
    /// <param name="pirmas">Pirmas Leidinio tipo objektas</param>
    /// <param name="antras">Antras leidinio tipo objektas</param>
    /// <returns>Grazinama true, jeigu antro pavadinimas didesnis ir atitinkamai atvirksciai</returns>
    static public bool operator <(Leidinys pirmas, Leidinys antras)
    {
        return !(pirmas > antras);
    }
}