﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Leidiniu saraso klase
/// </summary>
public sealed class LeidiniuSarasas
{
    // Klases LeidiniuMazgas aprasas
    private LeidiniuMazgas pr;                  // Saraso pradzia
    private LeidiniuMazgas pb;                  // Saraso pabaiga
    private LeidiniuMazgas d;                   // Saraso sasaja

    /// <summary>
    /// Funkcija nustatanti pradines saraso adresu reiksmes
    /// </summary>
    public LeidiniuSarasas()
    {
        pr = null;
        pb = null;
        d = null;
    }

    /// <summary>
    /// Sasajai priskiriama saraso pradzia
    /// </summary>
    public void Pradzia() { d = pr; }

    /// <summary>
    /// Funkcija priskirianti sarasui tolesni elementa
    /// </summary>
    public void Kitas() { d = d.Kitas; }

    /// <summary>
    /// Funkcija patikrinanti ar sarasas nera tuscias
    /// </summary>
    /// <returns>Grazinama true, jeigu sarasas ne tuscias, false atvirksciai</returns>
    public bool Yra()
    {
        return d != null;
    }

    /// <summary>
    /// Funkcija grazinanti sarasaso sasajos elemento reiksme
    /// </summary>
    /// <returns>Saraso sasajos elemento reiksme</returns>
    public Leidinys ImtiDuomenis()
    {
        return d.Duom;
    }

    /// <summary>
    /// Funkcija, kuri deda naujus leidinius i sarasa tiesiogine tvarka
    /// </summary>
    /// <param name="naujas">Leidinys, kuris bus idedamas</param>
    public void DetiDuomenisT(Leidinys naujas)
    {
        var dd = new LeidiniuMazgas(naujas, null);
        if (pr != null)
        {
            pb.Kitas = dd;
            pb = dd;
        }
        else
        {
            pr = dd;
            pb = dd;
        }
    }

    /// <summary>
    /// Funckija isrikiuojanti leidiniu sarasa pagal leidinio pavadinima abeceles tvarka
    /// </summary>
    public void Rikiuoti()
    {
        for (LeidiniuMazgas d1 = pr; d1 != null; d1 = d1.Kitas)
        {
            LeidiniuMazgas maxv = d1;

            for (LeidiniuMazgas d2 = d1; d2 != null; d2 = d2.Kitas)
            {
                if (d2.Duom < maxv.Duom)
                {
                    maxv = d2;
                }
            }

            // Informaciniu daliu sukeitimas vietomis
            Leidinys leidinys = d1.Duom;
            d1.Duom = maxv.Duom;
            maxv.Duom = leidinys;
        }
    }
}