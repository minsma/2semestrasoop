﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Prenumeratoriu saraso klase
/// </summary>
public sealed class PrenumeratoriuSarasas
{
    // Klases mazgas aprasas
    private PrenumeratoriuMazgas pr;                  // Saraso pradzia
    private PrenumeratoriuMazgas pb;                  // Saraso pabaiga
    private PrenumeratoriuMazgas d;                   // Saraso sasaja

    /// <summary>
    /// Pradines saraso adresu reiksmes
    /// </summary>
    public PrenumeratoriuSarasas()
    {
        pr = null;
        pb = null;
        d = null;
    }

    /// <summary>
    /// Sasajai priskiriama saraso pradzia
    /// </summary>
    public void Pradzia() { d = pr; }

    /// <summary>
    /// Sasajai priskiriamas saraso tolesnis elementas
    /// </summary>
    public void Kitas() { d = d.Kitas; }

    /// <summary>
    /// Funkcija tikrinanti ar sarasas nera tuscias
    /// </summary>
    /// <returns>Grazina true, jeigu sarasas netuscias</returns>
    public bool Yra()
    {
        return d != null;
    }

    /// <summary>
    /// Funkcija skirta paimti duomenims
    /// </summary>
    /// <returns>Grazina saraso sasajos elemento reiksme</returns>
    public Prenumeratorius ImtiDuomenis()
    {
        return d.Duom;
    }

    /// <summary>
    /// Funkcija skirta deti duomenis tiesiogine tvarka
    /// </summary>
    /// <param name="naujas">Naujas objektas</param>
    public void DetiDuomenisT(Prenumeratorius naujas)
    {
        var dd = new PrenumeratoriuMazgas(naujas, null);
        if (pr != null)
        {
            pb.Kitas = dd;
            pb = dd;
        }
        else
        {
            pr = dd;
            pb = dd;
        }
    }
}