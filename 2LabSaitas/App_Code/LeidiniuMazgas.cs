﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Leidiniu mazgo klase, kuri skirta susieti leidinius i bendra sarasa
/// </summary>
public sealed class LeidiniuMazgas
{
    public Leidinys Duom { get; set; }                  // Leidinio tipo objektas
    public LeidiniuMazgas Kitas { get; set; }           // Kitas objektas, i kuri bus rodoma

    /// <summary>
    /// Konstruktorius skirtas nustatyti pradinems klases objekto reiksmems
    /// </summary>
    /// <param name="reiksme">Leidinio tipo objektas</param>
    /// <param name="adr">Kitas objektas, i kuri bus rodoma</param>
    public LeidiniuMazgas(Leidinys reiksme, LeidiniuMazgas adr)
    {
        Duom = reiksme;
        Kitas = adr;
    }
}