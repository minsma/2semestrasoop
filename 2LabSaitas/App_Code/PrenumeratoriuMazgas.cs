﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Prenumeratoriaus mazgas skirtas susieti prenumeratoriaus tipo objektus tarpusavyje
/// </summary>
public sealed class PrenumeratoriuMazgas
{
    public Prenumeratorius Duom { get; set; }                   // Prenumeratoriaus tipo objektas
    public PrenumeratoriuMazgas Kitas { get; set; }             // Kitas objektas i kuri bus rodoma

    /// <summary>
    /// Konstruktorius nustatantis pradines reiksmes
    /// </summary>
    /// <param name="reiksme">Prenumeratoriaus tipo objektas</param>
    /// <param name="adr">Kitas objektas i kuri bus rodoma</param>
    public PrenumeratoriuMazgas(Prenumeratorius reiksme, PrenumeratoriuMazgas adr)
    {
        Duom = reiksme;
        Kitas = adr;
    }
}