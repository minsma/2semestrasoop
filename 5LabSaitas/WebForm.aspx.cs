﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class WebForma : System.Web.UI.Page
{
    // Rezultatu failo pavadinimas
    const string CDfr1 = "Results.txt";
    // Pradiniams duomenims perspausdinti lentele skirto failo pavadinimas
    const string CDfr2 = "PrimaryDataInTables.txt";

    // Krepsininku sarasas
    List<Player> playersList = new List<Player>();

    /// <summary>
    /// Funkcija, kurioje ivykdomi veiksmai pries paspaudziant mygtuka
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        LabelResults.Visible = false;

        if (!Directory.Exists(Server.MapPath("App_Data")))
        {
            throw new Exception("App_Data Directory nerasta.");
        }

        var matchDataFiles = Directory.GetFiles(Server.MapPath("App_Data"), "Rungtynes*.txt");

        try
        {
            playersList = ReadPlayersData(matchDataFiles);
        }
        catch
        {
            Response.Write("<script>alert(' Rungtynes* failai yra neskaitomi ')</script>");
            return;
        }

        var playersDataFiles = Directory.GetFiles(Server.MapPath("App_Data"), "Zaidejai*.txt");

        List<Player> playersPositionsList = new List<Player>();

        try
        {
            playersPositionsList = ReadPlayersData(playersDataFiles);
        }
        catch
        {
            Response.Write("<script>alert(' Zaidejai* failai yra neskaitomi ')</script>");
            return;
        }

        AddPositionForPlayers(playersList, playersPositionsList);

        PrintPlayersTableToWebSite(TableAllPlayers, playersList);

        PrintPlayersDataToFile(CDfr2, playersList);
    }

    /// <summary>
    /// Funkcija, kurioje pradedami vykdyti veiksmai po mygtuko paspaudimo
    /// </summary>
    protected void ButtonStart_Click(object sender, EventArgs e)
    {
        int mostValuablePlayersCount = 0;

        try
        {
            mostValuablePlayersCount = Convert.ToInt32(TextBoxMostValPlCount.Text);
        }
        catch (Exception ex)
        {
            LabelMostValPlCountEx.Text = ex.Message;
        }

        string mostValuablePlayersPosition = TextBoxMostValPlPosition.Text;

        playersList = playersList.OrderByDescending(nn => nn.PointsScored).
                                  ThenBy(nn => nn.MinutesPlayed).
                                  ThenBy(nn => nn.TurnoversMade).
                                  ToList<Player>();

        List<Player> mostValuablePlayersList = playersList.Where(nn => nn.Position == mostValuablePlayersPosition).
                                                           Take(mostValuablePlayersCount).
                                                           ToList<Player>();

        LabelResults.Visible = true;

        PrintPlayersTableToWebSite(TableMostValuablePlayers, mostValuablePlayersList);

        PrintPlayersDataToFile(CDfr1, mostValuablePlayersList);
    }

    /// <summary>
    /// Funkcija, kurioje yra nuskaitomi pradiniai duomenys
    /// </summary>
    /// <param name="dataFiles">Duomenu failu pavadinimai</param>
    /// <returns>Krepsininku sarasas</returns>
    protected List<Player> ReadPlayersData(string[] dataFiles)
    {
        List<Player> playerList = new List<Player>();

        for (int i = 0; i < dataFiles.Length; i++)
        {
            using (StreamReader reader = new StreamReader(@dataFiles[i]))
            {
                string line = null;

                while ((line = reader.ReadLine()) != null)
                {
                    if (line == null)
                    {
                        throw new Exception(string.Format("Wrong {i} data file format.", i + 1));
                    }
                    else
                    {
                        string[] values = line.Split(';');

                        if (values.Length == 5)
                        {
                            string teamName = values[0];
                            string lastName = values[1];
                            string firstName = values[2];
                            string position = values[3];

                            Player player = new Player(teamName, lastName, firstName, position);

                            playerList.Add(player);
                        }
                        else if (values.Length == 7)
                        {
                            string teamName = values[0];
                            string lastName = values[1];
                            string firstName = values[2];
                            int minutesPlayed = Convert.ToInt32(values[3]);
                            int pointsScored = Convert.ToInt32(values[4]);
                            int turnoversMade = Convert.ToInt32(values[5]);

                            Player player = new Player(teamName, lastName, firstName, minutesPlayed, pointsScored, turnoversMade);

                            playerList.Add(player);
                        }
                    }
                }
            }
        }

        return playerList;
    }

    /// <summary>
    /// Funkcija, kurioje krepsininkams yra pridedamos pozicijos
    /// </summary>
    /// <param name="players">Zaideju sarasas, be poziciju</param>
    /// <param name="playerPosition">Zaideju sarasas is kurio paimamos pozicijos</param>
    protected void AddPositionForPlayers(List<Player> players,
                                         List<Player> playerPosition)
    {
        var joined = players.Join(playerPosition, p => new { p.FirstName, p.LastName, p.TeamName },
                                  p => new { p.FirstName, p.LastName, p.TeamName },
                                  (player, playerPos) => new { player, playerPos });

        foreach (var pair in joined)
        {
            pair.player.Position = pair.playerPos.Position;
        }
    }

    /// <summary>
    /// Funkcija, spausdinanti zaideju duomenis lentele i svetaine
    /// </summary>
    /// <param name="table">Lenteles pavadinimas</param>
    /// <param name="players">Zaideju sarasas</param>
    protected void PrintPlayersTableToWebSite(Table table, List<Player> players)
    {
        TableRow row = new TableRow();

        TableCell teamName = new TableCell();
        teamName.Text = "Komandos pavadinimas";
        row.Cells.Add(teamName);

        TableCell lastName = new TableCell();
        lastName.Text = "Pavarde";
        row.Cells.Add(lastName);

        TableCell firstName = new TableCell();
        firstName.Text = "Vardas";
        row.Cells.Add(firstName);

        TableCell minutesPlayed = new TableCell();
        minutesPlayed.Text = "Zaistos minutes";
        row.Cells.Add(minutesPlayed);

        TableCell pointsScored = new TableCell();
        pointsScored.Text = "Pelnyti taskai";
        row.Cells.Add(pointsScored);

        TableCell turnoversMade = new TableCell();
        turnoversMade.Text = "Padarytos klaidos";
        row.Cells.Add(turnoversMade);

        TableCell position = new TableCell();
        position.Text = "Pozicija";
        row.Cells.Add(position);

        table.Rows.Add(row);

        foreach (Player player in players)
        {
            row = new TableRow();

            teamName = new TableCell();
            teamName.Text = player.TeamName;
            row.Cells.Add(teamName);

            lastName = new TableCell();
            lastName.Text = player.LastName;
            row.Cells.Add(lastName);

            firstName = new TableCell();
            firstName.Text = player.FirstName;
            row.Cells.Add(firstName);

            minutesPlayed = new TableCell();
            minutesPlayed.Text = player.MinutesPlayed.ToString();
            row.Cells.Add(minutesPlayed);

            pointsScored = new TableCell();
            pointsScored.Text = player.PointsScored.ToString();
            row.Cells.Add(pointsScored);

            turnoversMade = new TableCell();
            turnoversMade.Text = player.TurnoversMade.ToString();
            row.Cells.Add(turnoversMade);

            position = new TableCell();
            position.Text = player.Position;
            row.Cells.Add(position);

            table.Rows.Add(row);
        }
    }

    /// <summary>
    /// Funkcija, spausdinanti zaideju duomenis lentele i tekstini faila
    /// </summary>
    /// <param name="fw">Tekstinio failo pavadinimas</param>
    /// <param name="players">Zaideju sarasas</param>
    protected void PrintPlayersDataToFile(string fw, List<Player> players)
    {
        using (StreamWriter writer = new StreamWriter(Server.MapPath("App_Data/" + fw)))
        {
            writer.WriteLine(new string('-', 113));
            writer.WriteLine(string.Format("|{0, -20}|{1, -20}|{2, -20}|{3, -10}|" +
                                            "{4, -10}|{5, -10}|{6, -15}|", "Komandos pavadinimas",
                                                                           "Pavarde", "Vardas",
                                                                           "Minutes", "Taskai",
                                                                           "Klaidos", "Pozicija"));
            writer.WriteLine(new string('-', 113));
            foreach (Player player in players)
            {
                writer.WriteLine(player);
                writer.WriteLine(new string('-', 113));
            }
        }
    }

    /// <summary>
    /// Funkcija, po mygtuko paspaudimo perkraunanti puslapi
    /// </summary>
    protected void ButtonReload_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }
}