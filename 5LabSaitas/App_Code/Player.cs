﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Zaidejo klase
/// </summary>
public class Player
{
    public string TeamName { get; set; }   // Komandos pavadinimas
    public string LastName { get; set; }   // Pavarde
    public string FirstName { get; set; }  // Vardas
    public int MinutesPlayed { get; set; } // Praleistos minutes aiksteleje
    public int PointsScored { get; set; }  // Pelnyti taskai
    public int TurnoversMade { get; set; } // Padarytos klaidos
    public string Position { get; set; }   // Pozicija

    /// <summary>
    /// Tuscias konstruktorius
    /// </summary>
    public Player()
    {
    }

    /// <summary>
    /// Konstruktorius nustatantis pradines reiksmes, be pozicijos
    /// </summary>
    /// <param name="teamName">Komandos pavadinimas</param>
    /// <param name="lastName">Pavarde</param>
    /// <param name="firstName">Vardas</param>
    /// <param name="minutesPlayed">Suzaistos minutes</param>
    /// <param name="pointsScored">Pelnyti taskai</param>
    /// <param name="turnoversMade">Padaryta klaidu</param>
    public Player(string teamName, string lastName, string firstName,
                  int minutesPlayed, int pointsScored, int turnoversMade)
    {
        TeamName = teamName;
        LastName = lastName;
        FirstName = firstName;
        MinutesPlayed = minutesPlayed;
        PointsScored = pointsScored;
        TurnoversMade = turnoversMade;
    }

    /// <summary>
    /// Konstruktorius, kuriame nustatoma ir pozicija
    /// </summary>
    /// <param name="teamName">Komandos pavadinimas</param>
    /// <param name="lastName">Zaidejo pavarde</param>
    /// <param name="firstName">Zaidejo vardas</param>
    /// <param name="position">Zaidejo pozicija</param>
    public Player(string teamName, string lastName, string firstName,
                  string position)
    {
        TeamName = teamName;
        LastName = lastName;
        FirstName = firstName;
        Position = position;
    }

    /// <summary>
    /// Metodas, skirtas spausdinimui lentele
    /// </summary>
    /// <returns>Grazinama spausdinimui paruosta eilute</returns>
    public override string ToString()
    {
        return String.Format("|{0, -20}|{1, -20}|{2, -20}|{3, 10}|{4, 10}|{5, 10}|{6, -15}|",
                             TeamName, LastName, FirstName, MinutesPlayed, PointsScored,
                             TurnoversMade, Position);
    }
}