﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebForm.aspx.cs" 
         Inherits="WebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Naudingiausi krepšininkai</title>
    <link href="Styles\style.css" rel="stylesheet" type="text/css" 
          media="screen" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="LabelMostValPlCount" runat="server" 
                   Text="Įveskite, kiek norite gauti naudingiausių žaidėjų:"></asp:Label>
        <asp:TextBox ID="TextBoxMostValPlCount" runat="server"
                     CssClass="textBox"></asp:TextBox>
        <asp:Label ID="LabelMostValPlCountEx" runat="server"></asp:Label>
        <br />
        <asp:Label ID="LabelMostValPlPosition" runat="server" 
                   Text="Įveskite, kokios norite pozicijos gauti naudingiausius žaidėjus:"></asp:Label>
        <asp:TextBox ID="TextBoxMostValPlPosition" runat="server"
                     CssClass="textBox"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="LabelPrimaryData" runat="server" Text="Pradiniai duomenys:"></asp:Label>
        <br />
   
        <asp:Table ID="TableAllPlayers" runat="server" CssClass="table">
        </asp:Table>
    
        <br />
        <asp:Button ID="ButtonStart" runat="server" OnClick="ButtonStart_Click" 
                    Text="Vykdyti!" CssClass="button"/>
        <br />
        <br />
        <asp:Label ID="LabelResults" runat="server" Text="Rezultatai:"></asp:Label>
        <br />
    
        <asp:Table ID="TableMostValuablePlayers" runat="server" CssClass="table">
        </asp:Table>
    
    </div>
    </form>
</body>
</html>
