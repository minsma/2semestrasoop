﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Leidinio klase, kuri yra konteineris prenumeratoriu sarasui
/// </summary>
public class Leidinys : IComparable<Leidinys>, IEquatable<Leidinys>
{
    public string Kodas { get; set; }                                                       // Leidinio kodas
    public string Pavadinimas { get; set; }                                                 // Leidinio pavadinimas
    public double VienoMenesioKaina { get; set; }                                           // Vieno menesio leidinio kaida
    public Sarasas<Prenumeratorius> PrenumSarasas;                                          // Leidini prenumeruojanciu asmenu sarasas

    /// <summary>
    /// Tuscias konstruktorius
    /// </summary>
    public Leidinys() { }

    /// <summary>
    /// Konstruktorius, kuriame nustatomi pradiniai duomenys leidiniui
    /// </summary>
    /// <param name="kodas">Leidinio kodas</param>
    /// <param name="pavadinimas">Leidinio pavadinimas</param>
    /// <param name="vienoMenesioKaina">Leidinio vieno menesio prenumeratos kaina</param>
    public Leidinys(string kodas, string pavadinimas, double vienoMenesioKaina)
    {
        Kodas = kodas;
        Pavadinimas = pavadinimas;
        VienoMenesioKaina = vienoMenesioKaina;
        PrenumSarasas = new Sarasas<Prenumeratorius>();
    }

    /// <summary>
    /// Funkcija skirta paruosti duomenis spausdinimui
    /// </summary>
    /// <returns>Grazinama string tipo eilute su leidinio visais duomenimis</returns>
    public override string ToString()
    {
        return String.Format("|{0, -16}|{1,-21}|{2, 19}|", Kodas, Pavadinimas, VienoMenesioKaina);
    }

    /// <summary>
    /// > operatoriaus uzklojimas skirtas rikiavimui
    /// </summary>
    /// <param name="pirmas">Pirmas Leidinio tipo objektas</param>
    /// <param name="antras">Antras leidinio tipo objektas</param>
    /// <returns>Grazinama true, jeigu pirmo pavadinimas didesnis ir atitinkamai atvirksciai</returns>
    static public bool operator >(Leidinys pirmas, Leidinys antras)
    {
        return String.Compare(pirmas.Pavadinimas, antras.Pavadinimas, StringComparison.CurrentCulture) > 0;
    }

    /// <summary>
    /// > operatoriaus uzklojimas skirtas rikiavimui
    /// </summary>
    /// <param name="pirmas">Pirmas Leidinio tipo objektas</param>
    /// <param name="antras">Antras leidinio tipo objektas</param>
    /// <returns>Grazinama true, jeigu antro pavadinimas didesnis ir atitinkamai atvirksciai</returns>
    static public bool operator <(Leidinys pirmas, Leidinys antras)
    {
        return !(pirmas > antras);
    }

    /// <summary>
    /// Metodas skirtas palyginti leidinius pagal pavadinima ir kieki
    /// </summary>
    /// <param name="kitas">Kitas Leidinys tipo objektas</param>
    /// <returns>1, jei kitas objektas yra lygus null, 1, jeigu this pavadinimas arba kaina didesne uz kitas ir -1 jeigu atvirksciai</returns>
    public int CompareTo(Leidinys kitas)
    {
        if(kitas == null)
        {
            return 1;
        }
        if(Pavadinimas.CompareTo(kitas.Pavadinimas) != 0)
        {
            return Pavadinimas.CompareTo(kitas.Pavadinimas);
        }
        else
        {
            return VienoMenesioKaina.CompareTo(kitas.VienoMenesioKaina);
        }
    }

    /// <summary>
    /// Metodas tikrinantis ar Leidinys tipo objektai nera vienodi
    /// </summary>
    /// <param name="kitas"></param>
    /// <returns></returns>
    public bool Equals(Leidinys kitas)
    {
        if(kitas == null)
        {
            return false;
        }
        if(Pavadinimas == kitas.Pavadinimas && VienoMenesioKaina == kitas.VienoMenesioKaina && Kodas == kitas.Kodas)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}