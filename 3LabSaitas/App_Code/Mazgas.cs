﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Mazgo klase
/// </summary>
public sealed class Mazgas<tipas>
{
    public tipas Duom { get; set; }                     // Leidinio tipo objektas
    public Mazgas<tipas> Kaire { get; set; }            // Sasajos ejimas i kaire
    public Mazgas<tipas> Desine { get; set; }           // Sasajos ejimas i desine

    /// <summary>
    /// Tuscias konstruktorius
    /// </summary>
    public Mazgas()
    {
    }

    /// <summary>
    /// Konstruktorius skirtas nustatyti pradinems klases objekto reiksmems
    /// </summary>
    /// <param name="reiksme">Leidinio tipo objektas</param>
    /// <param name="adr">Kitas objektas, i kuri bus rodoma</param>
    public Mazgas(tipas reiksme, Mazgas<tipas> adrk, Mazgas<tipas> adrd)
    {
        Duom = reiksme;
        Kaire = adrk;
        Desine = adrd;
    }
}