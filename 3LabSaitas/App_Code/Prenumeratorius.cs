﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Prenumeratoriaus klase
/// </summary>
public class Prenumeratorius : IComparable<Prenumeratorius>, IEquatable<Prenumeratorius>
{
    public string Pavarde { get; set; }                                                    // Prenumeratoriaus pavarde
    public string Adresas { get; set; }                                                    // Prenumeratoriaus adresas
    public int LaikotarpioPradzia { get; set; }                                            // Prenumeratoriaus leidinio prenumeratos laikotarpio pradzia
    public int LaikotarpioIlgis { get; set; }                                              // Prenumeratoriaus leidinio prenumeratos ilgis
    public string LeidinioKodas { get; set; }                                              // Prenumeratoriaus prenumeruojamo leidinio kodas
    public int LeidiniuKiekis { get; set; }                                                // Prenumeratoriaus prenumeruojamu leidiniu kiekis
    public Prenumeratorius Kitas { get; set; }                                             // Kitas saraso narys

    /// <summary>
    /// Tuscias konstruktorius
    /// </summary>
    public Prenumeratorius() { }

    /// <summary>
    /// Konstruktorius nustatantis pradines prenumeratoriaus reiksmes
    /// </summary>
    /// <param name="pavarde">Prenumeratoriaus pavarde</param>
    /// <param name="adresas">Prenumeratoriaus adresas</param>
    /// <param name="laikotarpioPradzia">Prenumeratos laikotarpio pradzia</param>
    /// <param name="laikotarpioIlgis">Prenumeratos laikotarpio ilgis</param>
    /// <param name="leidinioKodas">Prenumeruojamo leidinio kodas</param>
    /// <param name="leidiniuKiekis">Prenumeruojamo leidinio kiekis</param>
    public Prenumeratorius(string pavarde, string adresas, int laikotarpioPradzia,
                            int laikotarpioIlgis, string leidinioKodas, int leidiniuKiekis)
    {
        Pavarde = pavarde;
        Adresas = adresas;
        LaikotarpioPradzia = laikotarpioPradzia;
        LaikotarpioIlgis = laikotarpioIlgis;
        LeidinioKodas = leidinioKodas;
        LeidiniuKiekis = leidiniuKiekis;
    }

    /// <summary>
    /// Funkcija skirta parengti string tipo eilute duomenu isvedimui
    /// </summary>
    /// <returns>String tipo eilute su visais prenumeratoriaus duomenimis</returns>
    public override string ToString()
    {
        return String.Format("|{0, -22}|{1, -33}|{2, 15}|{3, 8}|{4, -14}|{5, 8}|", Pavarde, Adresas, LaikotarpioPradzia,
                                                                                  LaikotarpioIlgis, LeidinioKodas, LeidiniuKiekis);
    }

    /// <summary>
    /// Metodas palyginantis Prenumeratorius tipo objektus tarpusavyje pagal pavadinima ir prenumeruojamu leidiniu kieki
    /// </summary>
    /// <param name="kitas"></param>
    /// <returns></returns>
    public int CompareTo(Prenumeratorius kitas)
    {
        if(kitas == null)
        {
            return 1;
        }
        if(Pavarde.CompareTo(kitas.Pavarde) != 0)
        {
            return Pavarde.CompareTo(kitas.Pavarde);
        }
        else
        {
            return LeidiniuKiekis.CompareTo(LeidiniuKiekis);
        }
    }

    /// <summary>
    /// Metodas patikrinantis ar objektai yra lygus
    /// </summary>
    /// <param name="kitas">Prenumeratorius tipo objektas su kuriuo bus lyginama</param>
    /// <returns></returns>
    public bool Equals(Prenumeratorius kitas)
    {
        if(kitas == null)
        {
            return false;
        }
        if(Pavarde == kitas.Pavarde && Adresas == kitas.Adresas && LaikotarpioPradzia == kitas.LaikotarpioPradzia &&
           LaikotarpioIlgis == kitas.LaikotarpioIlgis && LeidinioKodas == kitas.LeidinioKodas && LeidiniuKiekis == kitas.LeidiniuKiekis)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}