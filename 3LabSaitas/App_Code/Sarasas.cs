﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Saraso klase
/// </summary>
public sealed class Sarasas<tipas> : IEnumerable<tipas> where tipas : IComparable<tipas>, IEquatable<tipas>
{
    // Klases LeidiniuMazgas aprasas
    private Mazgas<tipas> pr;                  // Saraso pradzia
    private Mazgas<tipas> pb;                  // Saraso pabaiga
    private Mazgas<tipas> d;                   // Saraso sasaja

    /// <summary>
    /// Funkcija nustatanti pradines saraso adresu reiksmes
    /// </summary>
    public Sarasas()
    {
        pr = null;
        pb = null;
        d = null;
    }

    /// <summary>
    /// Sasajai priskiriama saraso pradzia
    /// </summary>
    public void Pradzia()
    {
        d = pr;
    }

    /// <summary>
    /// Sasajai priskiriamas sekantis saraso elementas
    /// </summary>
    public void Kitas()
    {
        d = d.Desine;
    }

    /// <summary>
    /// Sasajai priskiriama saraso pabaiga
    /// </summary>
    public void Pabaiga()
    {
        d = pb;
    }

    /// <summary>
    /// Sasajai priskiriama null reiksme
    /// </summary>
    bool Baigti()
    {
        return d == null;
    }

    /// <summary>
    /// Pereinama per viena elementa i desine
    /// </summary>
    void Desinen()
    {
        if(d != null)
        {
            d = d.Desine;
        }
    }

    /// <summary>
    /// Pereinama per viena elementa i kaire
    /// </summary>
    void Kairen()
    {
        if(d != null)
        {
            d = d.Kaire;
        }
    }
    
    /// <summary>
    /// Funkcija patikrinanti ar sarasas nera tuscias
    /// </summary>
    /// <returns>Grazinama true, jeigu sarasas ne tuscias, false atvirksciai</returns>
    public bool Yra()
    {
        return d != null;
    }

    /// <summary>
    /// Funkcija grazinanti sarasaso sasajos elemento reiksme
    /// </summary>
    /// <returns>Saraso sasajos elemento reiksme</returns>
    public tipas ImtiDuomenis()
    {
        return d.Duom;
    }

    /// <summary>
    /// Funkcija, kuri deda naujus leidinius i sarasa tiesiogine tvarka
    /// </summary>
    /// <param name="naujas">Leidinys, kuris bus idedamas</param>
    public void DetiDuomenisT(tipas naujas)
    {
        var dd = new Mazgas<tipas>(naujas, pb, null);

        if (pr != null)
        {
            pb.Desine = dd;
        }
        else
        {
            pr = dd;
        }

        pb = dd;
    }

    /// <summary>
    /// Metodas isrikiuojantis leidiniu sarasa pagal leidinio pavadinima abeceles tvarka
    /// </summary>
    public void Rikiuoti()
    {
        for (Mazgas<tipas> d1 = pr; d1 != null; d1 = d1.Desine)
        {
            Mazgas<tipas> maxv = d1;

            for (Mazgas<tipas> d2 = d1; d2 != null; d2 = d2.Desine)
            {
                if (d2.Duom.CompareTo(maxv.Duom) < 0)
                {
                    maxv = d2;
                }
            }

            // Informaciniu daliu sukeitimas vietomis
            tipas leidinys = d1.Duom;
            d1.Duom = maxv.Duom;
            maxv.Duom = leidinys;
        }
    }

    public IEnumerator<tipas> GetEnumerator()
    {
        for (Mazgas<tipas> dd = pr; dd != null; dd = dd.Desine)
        {
            yield return dd.Duom;
        }
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
        throw new NotImplementedException();
    }
}