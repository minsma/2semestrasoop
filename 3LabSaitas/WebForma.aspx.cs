﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebForma : System.Web.UI.Page
{
    const string CDfw = "Rez.txt";                                                              // Galutiniu rezultatus tekstinis failas
    const string CDfw2 = "PradiniaiLeidiniai.txt";                                              // Pradiniui leidiniu sarasui perspausdinti lentele skirtas tekstinis failas
    const string CDfw3 = "PradiniaiPrenumeratoriai.txt";                                        // Pradiniui prenumeratoriu sarasui perspausdinti lentele skirtas tekstinis failas


    /// <summary>
    /// Funkcija, kuri po VykdymoButton paspaudimo ivykdo visus programos veiksmus
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click(object sender, EventArgs e)
    {
        Sarasas<Leidinys> leidiniuSarasas = SkaitymasLeidiniu(LeidFileUpload.FileName);
        Sarasas<Prenumeratorius> prenumeratoriuSarasas = SkaitymasPrenumeratoriu(PrenumFileUpload.FileName, leidiniuSarasas);

        SpausdinimasPradiniuDuomenu<Leidinys>(CDfw2, leidiniuSarasas, "| Leidinio kodas | Leidinio Pavadinimas | Kaina per menesi |");
        string eilute = string.Format("| {0, -20} | Prenumeratos Pristatymo Adresas | Pradzios Data | Trukme | {1, -12} | Kiekis |", "Pavarde", "Kodas");
        SpausdinimasPradiniuDuomenu<Prenumeratorius>(CDfw3, prenumeratoriuSarasas, eilute);

        double bendrosLeidiniuPajamos = BendrosiosLeidiniuPajamos(leidiniuSarasas);
        double vidurkis = VidutinesLeidinioPajamos(leidiniuSarasas);

        BLeidiniuPajTextBox.Text = Convert.ToString(bendrosLeidiniuPajamos);

        Sarasas<Leidinys> leidiniaiZemiauVidurkio = LeidiniaiZemiauVidurkio(leidiniuSarasas, vidurkis);

        leidiniaiZemiauVidurkio.Rikiuoti();

        Sarasas<Leidinys> daugiausiaiPajamuLeidiniai = KiekvienoMenesioDidziausiosPajamos(leidiniuSarasas);

        Sarasas<Prenumeratorius> atrinktuSarasas = NurodytuDuomenuPrenumeratoriuSarasas(leidiniuSarasas);

        Spausdinimas(CDfw, daugiausiaiPajamuLeidiniai, bendrosLeidiniuPajamos, leidiniaiZemiauVidurkio, atrinktuSarasas);

        MenesiuLentelesFormavimas(MenesiuTable, daugiausiaiPajamuLeidiniai);
        LeidiniuLentelesFormavimas(LeidMazUzVidTable, leidiniaiZemiauVidurkio);
        PrenumeratoriuLentelesFormavimas(PrenumSarPagalMenIrPavTable, atrinktuSarasas);

        RezElementuMatomumas(true);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RezElementuMatomumas(false);
    }

    /// <summary>
    /// Funkcija nuskaitanti duomenis is pradiniu duomenu tekstinio failo i leidiniu sarasa
    /// </summary>
    /// <param name="fr">Pradiniu duomenu failo pavadinimas</param>
    /// <returns>Grazinamas leidiniu sarasas</returns>
    protected Sarasas<Leidinys> SkaitymasLeidiniu(string fr)
    {
        Sarasas<Leidinys> sarasas = new Sarasas<Leidinys>();

        string line = null;

        using (StreamReader reader = new StreamReader(Server.MapPath("App_Data/" + fr)))
        {
            while ((line = reader.ReadLine()) != null)
            {
                var reiksmes = line.Split(';');
                string kodas = reiksmes[0];
                string pavadinimas = reiksmes[1];
                double vienoMenKaina = Convert.ToDouble(reiksmes[2]);

                var leidinys = new Leidinys(kodas, pavadinimas, vienoMenKaina);

                sarasas.DetiDuomenisT(leidinys);
            }
        }

        return sarasas;
    }

    /// <summary>
    /// Funkcija nuskaitantis duomenis is pradiniu duomenu tekstinio failo i leidiniu sarasa, 
    /// kuriame prenumeratoriai yra priskiriami atitinkamiems prenumeratoriams, pagal leidinio koda 
    /// </summary>
    /// <param name="fr">Pradiniu duomenu tekstinio failo pavadinimas</param>    
    /// <param name="leidiniuSarasas">Leidiniu sarasas</param>
    /// <returns>Grazinamas prenumeratoriu sarasas, si reiksme grazinama todel, kad 
    ///          reikia atspausdinti pradinius prenumeratoriu duomenis lentele</returns>
    protected Sarasas<Prenumeratorius> SkaitymasPrenumeratoriu(string fr, Sarasas<Leidinys> leidiniuSarasas)
    {
        Sarasas<Prenumeratorius> sarasas = new Sarasas<Prenumeratorius>();

        string line = null;

        using (StreamReader reader = new StreamReader(Server.MapPath("App_Data/" + fr)))
        {
            while ((line = reader.ReadLine()) != null)
            {
                var reiksmes = line.Split(';');
                string pavarde = reiksmes[0];
                string adresas = reiksmes[1];
                int laikotarpioPradzia = Convert.ToInt32(reiksmes[2]);
                int laikotarpioIlgis = Convert.ToInt32(reiksmes[3]);
                string leidinioKodas = reiksmes[4];
                int leidiniuKiekis = Convert.ToInt32(reiksmes[5]);

                Prenumeratorius prenumeratorius = new Prenumeratorius(pavarde, adresas, laikotarpioPradzia, laikotarpioIlgis, leidinioKodas, leidiniuKiekis);

                for (leidiniuSarasas.Pradzia(); leidiniuSarasas.Yra(); leidiniuSarasas.Kitas())
                {
                    if (prenumeratorius.LeidinioKodas == leidiniuSarasas.ImtiDuomenis().Kodas)
                    {
                        leidiniuSarasas.ImtiDuomenis().PrenumSarasas.DetiDuomenisT(prenumeratorius);
                    }
                }

                sarasas.DetiDuomenisT(prenumeratorius);
            }
        }

        return sarasas;
    }

    /// <summary>
    /// Funkcija spausdinanti pradinius duomenis i tekstinius failus
    /// </summary>
    /// <typeparam name="tipas">Duomenu tipas</typeparam>
    /// <param name="fw">Failo pavadinimas</param>
    /// <param name="sarasas">Sarasas is kurio bus spausdinama</param>
    /// <param name="eil">Lenteles 1 eilute</param>
    protected void SpausdinimasPradiniuDuomenu<tipas>(string fw, IEnumerable<tipas> sarasas, string eil) 
                                                      where tipas : IComparable<tipas>, IEquatable<tipas>
    {
        using (StreamWriter writer = new StreamWriter(Server.MapPath("App_Data/" + fw)))
        {
            writer.WriteLine(new string('-', eil.Length));
            writer.WriteLine(eil);
            writer.WriteLine(new string('-', eil.Length));

            foreach (tipas pirmas in sarasas)
            {
                writer.WriteLine(pirmas);
                writer.WriteLine(new string('-', eil.Length));
            }
        }
    }

    /// <summary>
    /// Funkcija susumuojanti bendras visu prenumeruojamu leidiniu pajamas, leidinio pajamos apskaiciuojamos
    /// vienam prenumeratoriui padauginus menesio kaina, prenumeruojama leidinio kieki ir leidinio trukme
    /// </summary>
    /// <param name="leidiniuSarasas">Leidiniu sarasas</param>
    /// <returns>Grazinaos bendros visu prenumeruojamu leidiniu pajamos</returns>
    protected double BendrosiosLeidiniuPajamos(Sarasas<Leidinys> leidiniuSarasas)
    {
        double suma = 0;

        for (leidiniuSarasas.Pradzia(); leidiniuSarasas.Yra(); leidiniuSarasas.Kitas())
        {
            for (leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Pradzia(); leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Yra(); leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Kitas())
            {
                suma += leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LeidiniuKiekis * 
                        leidiniuSarasas.ImtiDuomenis().VienoMenesioKaina * 
                        leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LaikotarpioIlgis;
            }
        }

        return suma;
    }

    /// <summary>
    /// Funkcija apskaiciuojanti leidiniu pajamu vidurki
    /// </summary>
    /// <param name="leidiniuSarasas">Leidiniu sarasas</param>
    /// <returns><Visu leidiniu pajamu vidurkis/returns>
    protected double VidutinesLeidinioPajamos(Sarasas<Leidinys> leidiniuSarasas)
    {
        int kiekis = 0;
        double suma = 0;

        for (leidiniuSarasas.Pradzia(); leidiniuSarasas.Yra(); leidiniuSarasas.Kitas())
        {
            kiekis++;

            for (leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Pradzia(); leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Yra(); leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Kitas())
            {
                suma += leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LeidiniuKiekis *
                        leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LaikotarpioIlgis *
                        leidiniuSarasas.ImtiDuomenis().VienoMenesioKaina;
            }
        }

        return (double)suma / kiekis;
    }

    /// <summary>
    /// Funkcija atrenkanti leidinius, kuriu pajamos yra zemiau bendro visu leidiniu vidurkio
    /// </summary>
    /// <param name="leidiniuSarasas">Leidiniu sarasas</param>
    /// <param name="vidurkis">Bendras visu leidiniu vidurkis</param>
    /// <returns>Leidiniu, kuriu pajamos zemiau bendro vidurkio sarasas</returns>
    protected Sarasas<Leidinys> LeidiniaiZemiauVidurkio(Sarasas<Leidinys> leidiniuSarasas, double vidurkis)
    {
        Sarasas<Leidinys> leidiniaiZemiauVidurkio = new Sarasas<Leidinys>();

        for (leidiniuSarasas.Pradzia(); leidiniuSarasas.Yra(); leidiniuSarasas.Kitas())
        {
            double suma = 0;

            for (leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Pradzia(); leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Yra(); leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Kitas())
            {
                suma += leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LeidiniuKiekis * leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LaikotarpioIlgis * leidiniuSarasas.ImtiDuomenis().VienoMenesioKaina;
            }

            if (suma < vidurkis)
            {
                leidiniaiZemiauVidurkio.DetiDuomenisT(leidiniuSarasas.ImtiDuomenis());
            }
        }

        return leidiniaiZemiauVidurkio;
    }

    /// <summary>
    /// Funkcija surandanti daugiausiai pajamu generuojanti leidini kiekviena menesi
    /// </summary>
    /// <param name="leidiniuSarasas">Leidiniu sarasas</param>
    /// <returns>Kiekvieno menesio daugiausiai sugeneravusiu pajamu leidiniu sarasas</returns>
    protected Sarasas<Leidinys> KiekvienoMenesioDidziausiosPajamos(Sarasas<Leidinys> leidiniuSarasas)
    {
        Sarasas<Leidinys> menesiai = new Sarasas<Leidinys>();

        for (int i = 1; i <= 12; i++)
        {
            double didz = 0;
            Leidinys didziausias = new Leidinys();

            for (leidiniuSarasas.Pradzia(); leidiniuSarasas.Yra(); leidiniuSarasas.Kitas())
            {
                double suma = 0;

                for (leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Pradzia(); leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Yra(); leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Kitas())
                {
                    if (leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LaikotarpioPradzia <= i &&
                        leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LaikotarpioIlgis + leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LaikotarpioPradzia - 1 >= i)
                    {
                        suma += leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LeidiniuKiekis * leidiniuSarasas.ImtiDuomenis().VienoMenesioKaina;
                    }
                }

                if (suma > didz)
                {
                    didz = suma;
                    didziausias = leidiniuSarasas.ImtiDuomenis();
                }
            }

            menesiai.DetiDuomenisT(didziausias);
        }

        return menesiai;
    }

    ///// <summary>
    ///// Funkcija sudaranti prenumeratoriu sarasa, pagal svetaineje pateiktus duomenis LeidinioPavTextBox ir LeidinioMenNrTextBox laukeliuose
    ///// </summary>
    ///// <param name="leidiniuSarasas">Leidiniu sarasas</param>
    ///// <returns>Atrinktu pagal pateiktus duomenis prenumeratoriu sarasas</returns>
    protected Sarasas<Prenumeratorius> NurodytuDuomenuPrenumeratoriuSarasas(Sarasas<Leidinys> leidiniuSarasas)
    {
        Sarasas<Prenumeratorius> atrinktuSarasas = new Sarasas<Prenumeratorius>();

        string pavadinimas = LeidinioPavTextBox.Text;

        int menesis = Convert.ToInt32(LeidinioMenNrTextBox.Text);

        for (leidiniuSarasas.Pradzia(); leidiniuSarasas.Yra(); leidiniuSarasas.Kitas())
        {
            for (leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Pradzia(); leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Yra(); leidiniuSarasas.ImtiDuomenis().PrenumSarasas.Kitas())
            {
                if (leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LaikotarpioPradzia <= menesis &&
                   leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LaikotarpioIlgis + leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis().LaikotarpioPradzia > menesis &&
                   leidiniuSarasas.ImtiDuomenis().Pavadinimas == pavadinimas)
                {
                    atrinktuSarasas.DetiDuomenisT(leidiniuSarasas.ImtiDuomenis().PrenumSarasas.ImtiDuomenis());
                }
            }
        }

        return atrinktuSarasas;
    }

    ///// <summary>
    ///// Rezultatu spausdinimas i tekstini faila
    ///// </summary>
    ///// <param name="fw">Tekstinio failo pavadinimas i kuri bus spausdinama</param>
    ///// <param name="KiekvienoMenesioDidziausiosPajamos">Kiekvieno menesio daugiausiai surinkusiu pajamu leidiniu sarasas</param>
    ///// <param name="bendrosiosLeidiniuPajamos">Bendros visu prenumeruojamu leidiniu surinktos pajamos</param>
    ///// <param name="leidiniaiZemiauVidurkio">Prenumeruojamu leidiniu, kurie surinko maziau nei bendras vidurkis sarasas, 
    /////                                       kuriame taip pat yra ir leidiniai, 
    /////                                       kurie visiskai nesurinko pajamu, nes yra neprenumeruojami</param>
    ///// <param name="nurodytuDuomenuPrenumeratoriuSarasas">Prenumeratoriu sarasas, kurie buvo atrinkti pagal zurnalo pavadinima
    /////                                                    ir prenumeratos menesi</param>
    protected void Spausdinimas(string fw, Sarasas<Leidinys> KiekvienoMenesioDidziausiosPajamos, double bendrosiosLeidiniuPajamos, Sarasas<Leidinys> leidiniaiZemiauVidurkio,
                             Sarasas<Prenumeratorius> nurodytuDuomenuPrenumeratoriuSarasas)
    {
        using (StreamWriter writer = new StreamWriter(Server.MapPath("App_Data/" + fw)))
        {
            writer.WriteLine(new string('-', 29));
            writer.WriteLine("|Menesis|Zurnalo pavadinimas|");
            writer.WriteLine(new string('-', 29));

            int i = 1;

            for (KiekvienoMenesioDidziausiosPajamos.Pradzia(); KiekvienoMenesioDidziausiosPajamos.Yra(); KiekvienoMenesioDidziausiosPajamos.Kitas())
            {
                writer.WriteLine("|{0, 7}|{1, -19}|", i, KiekvienoMenesioDidziausiosPajamos.ImtiDuomenis().Pavadinimas);
                i++;
            }
            writer.WriteLine(new string('-', 29));

            writer.WriteLine();

            writer.WriteLine("Bendrosios leidiniu pajamos: {0}", bendrosiosLeidiniuPajamos);

            writer.WriteLine();

            writer.WriteLine(new string('-', 60));
            writer.WriteLine("| Leidinio kodas | Leidinio Pavadinimas | Kaina per menesi |");
            writer.WriteLine(new string('-', 60));

            for (leidiniaiZemiauVidurkio.Pradzia(); leidiniaiZemiauVidurkio.Yra(); leidiniaiZemiauVidurkio.Kitas())
            {
                writer.WriteLine(leidiniaiZemiauVidurkio.ImtiDuomenis());
                writer.WriteLine(new string('-', 60));
            }

            writer.WriteLine();

            writer.WriteLine(new string('-', 107));
            writer.WriteLine("| {0, -20} | Prenumeratos Pristatymo Adresas | Pradzios Data | Trukme | {1, -12} | Kiekis |", "Pavarde", "Kodas");
            writer.WriteLine(new string('-', 107));

            for (nurodytuDuomenuPrenumeratoriuSarasas.Pradzia(); nurodytuDuomenuPrenumeratoriuSarasas.Yra(); nurodytuDuomenuPrenumeratoriuSarasas.Kitas())
            {
                writer.WriteLine(nurodytuDuomenuPrenumeratoriuSarasas.ImtiDuomenis());
                writer.WriteLine(new string('-', 107));
            }
        }
    }

    /// <summary>
    /// Funkcija formuojanti leidiniu saraso lentele grafineje vartotojo sasajoje
    /// </summary>
    /// <param name="lentele">Lentele</param>
    /// <param name="leidiniuSarasas">Leidiniu sarasas</param>
    protected void LeidiniuLentelesFormavimas(Table lentele, Sarasas<Leidinys> leidiniuSarasas)
    {
        TableRow row = new TableRow();

        TableCell kodas = new TableCell();
        kodas.Text = "<b>Leidinio Kodas<b>";
        row.Cells.Add(kodas);

        TableCell pavadinimas = new TableCell();
        pavadinimas.Text = "<b>Leidinio Pavadinimas<b>";
        row.Cells.Add(pavadinimas);

        TableCell kaina = new TableCell();
        kaina.Text = "<b>Vieno menesio leidinio kaina<b>";
        row.Cells.Add(kaina);

        lentele.Rows.Add(row);

        for (leidiniuSarasas.Pradzia(); leidiniuSarasas.Yra(); leidiniuSarasas.Kitas())
        {
            row = new TableRow();

            kodas = new TableCell();
            kodas.Text = leidiniuSarasas.ImtiDuomenis().Kodas;
            row.Cells.Add(kodas);

            pavadinimas = new TableCell();
            pavadinimas.Text = leidiniuSarasas.ImtiDuomenis().Pavadinimas;
            row.Cells.Add(pavadinimas);

            kaina = new TableCell();
            kaina.Text = Convert.ToString(leidiniuSarasas.ImtiDuomenis().VienoMenesioKaina);
            row.Cells.Add(kaina);

            lentele.Rows.Add(row);
        }
    }  

    /// <summary>
    /// Funkcija formuojanti atrinktu prenumeratoriu saraso lentele grafineje vartotojo sasajoje
    /// </summary>
    /// <param name="lentele">Lentele</param>
    /// <param name="prenumeratoriuSarasas">Prenumeratoriu sarasas</param>
    protected void PrenumeratoriuLentelesFormavimas(Table lentele, Sarasas<Prenumeratorius> prenumeratoriuSarasas)
    {
        TableRow row = new TableRow();

        TableCell pavarde = new TableCell();
        pavarde.Text = "<b>Pavarde<b>";
        row.Cells.Add(pavarde);

        TableCell adresas = new TableCell();
        adresas.Text = "<b>Adresas<b>";
        row.Cells.Add(adresas);

        TableCell pradzia = new TableCell();
        pradzia.Text = "<b>Laikotarpio pradžia<b>";
        row.Cells.Add(pradzia);

        TableCell ilgis = new TableCell();
        ilgis.Text = "<b>Laikotarpio ilgis<b>";
        row.Cells.Add(ilgis);

        TableCell kodas = new TableCell();
        kodas.Text = "<b>Leidinio kodas<b>";
        row.Cells.Add(kodas);

        TableCell kiekis = new TableCell();
        kiekis.Text = "<b>Leidinių kiekis<b>";
        row.Cells.Add(kiekis);

        lentele.Rows.Add(row);

        for(prenumeratoriuSarasas.Pradzia(); prenumeratoriuSarasas.Yra(); prenumeratoriuSarasas.Kitas())
        {
            row = new TableRow();

            pavarde = new TableCell();
            pavarde.Text = prenumeratoriuSarasas.ImtiDuomenis().Pavarde;
            row.Cells.Add(pavarde);

            adresas = new TableCell();
            adresas.Text = prenumeratoriuSarasas.ImtiDuomenis().Adresas;
            row.Cells.Add(adresas);

            pradzia = new TableCell();
            pradzia.Text = Convert.ToString(prenumeratoriuSarasas.ImtiDuomenis().LaikotarpioPradzia);
            row.Cells.Add(pradzia);

            ilgis = new TableCell();
            ilgis.Text = Convert.ToString(prenumeratoriuSarasas.ImtiDuomenis().LaikotarpioIlgis);
            row.Cells.Add(ilgis);

            kodas = new TableCell();
            kodas.Text = prenumeratoriuSarasas.ImtiDuomenis().LeidinioKodas;
            row.Cells.Add(kodas);

            kiekis = new TableCell();
            kiekis.Text = Convert.ToString(prenumeratoriuSarasas.ImtiDuomenis().LeidiniuKiekis);
            row.Cells.Add(kiekis);

            lentele.Rows.Add(row);
        }
    }

    /// <summary>
    /// Funkcija formuojanti daugiausiai pajamu generuojanciu leidiniu pagal menesi saraso lentele grafineje vartotojo sasajoje
    /// </summary>
    /// <param name="lentele">Lentele</param>
    /// <param name="menesiai">Daugiausiai pajamu generuojanciu leidiniu sarasas</param>
    protected void MenesiuLentelesFormavimas(Table lentele, Sarasas<Leidinys> daugiausiaiPajamuLeidiniai)
    {
        TableRow row = new TableRow();

        TableCell menesioSkaicius = new TableCell();
        menesioSkaicius.Text = "<b>Menesio skaicius<b>";
        row.Cells.Add(menesioSkaicius);

        TableCell leidinioPavadinimas = new TableCell();
        leidinioPavadinimas.Text = "<b>Leidinio pavadinimas<b>";
        row.Cells.Add(leidinioPavadinimas);

        lentele.Rows.Add(row);

        int i = 1;  // kintamasis naudojamas menesio skaiciaus isvedimui

        for(daugiausiaiPajamuLeidiniai.Pradzia(); daugiausiaiPajamuLeidiniai.Yra(); daugiausiaiPajamuLeidiniai.Kitas())
        {
            row = new TableRow();

            menesioSkaicius = new TableCell();
            menesioSkaicius.Text = Convert.ToString(i++);
            row.Cells.Add(menesioSkaicius);

            leidinioPavadinimas = new TableCell();
            leidinioPavadinimas.Text = daugiausiaiPajamuLeidiniai.ImtiDuomenis().Pavadinimas;
            row.Cells.Add(leidinioPavadinimas);

            lentele.Rows.Add(row);
        }
    }

    /// <summary>
    /// Funkcija nustatanti formos elementu, kuriuose isvedami rezultatai matomuma
    /// </summary>
    /// <param name="arMatomas">true arba false reiksme</param>
    protected void RezElementuMatomumas(bool arMatomas)
    {
        MenesiuSarasoLabel.Visible = arMatomas;
        MenesiuTable.Visible = arMatomas;
        BLeidPajLabel.Visible = arMatomas;
        BLeidiniuPajTextBox.Visible = arMatomas;
        LeidMazUzVidLabel.Visible = arMatomas;
        LeidMazUzVidTable.Visible = arMatomas;
        PrenumSarPagalMenIrPavLabel.Visible = arMatomas;
        PrenumSarPagalMenIrPavTable.Visible = arMatomas;
    }
}
