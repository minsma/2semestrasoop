﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebForma.aspx.cs" Inherits="WebForma" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Prenumeratos</title>
    <link href="/Styles/style.css" rel="stylesheet" type="text/css" media="screen" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <div class ="pradiniai">

        <asp:Label ID="U3afailoLabel" runat="server" Text="U3a failas"></asp:Label>
        <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/App_Data/U3a.xml"></asp:XmlDataSource>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="XmlDataSource1" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="kodas" HeaderText="kodas" SortExpression="kodas" />
                <asp:BoundField DataField="pavadinimas" HeaderText="pavadinimas" SortExpression="pavadinimas" />
                <asp:BoundField DataField="kaina" HeaderText="kaina" SortExpression="kaina" />
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
        <br />
        <asp:Label ID="U3bfailoLabel" runat="server" Text="U3b failas"></asp:Label>
        <asp:XmlDataSource ID="XmlDataSource2" runat="server" DataFile="~/App_Data/U3b.xml"></asp:XmlDataSource>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="XmlDataSource2" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="pavarde" HeaderText="pavarde" SortExpression="pavarde" />
                <asp:BoundField DataField="adresas" HeaderText="adresas" SortExpression="adresas" />
                <asp:BoundField DataField="pradzia" HeaderText="pradzia" SortExpression="pradzia" />
                <asp:BoundField DataField="ilgis" HeaderText="ilgis" SortExpression="ilgis" />
                <asp:BoundField DataField="kodas" HeaderText="kodas" SortExpression="kodas" />
                <asp:BoundField DataField="kiekis" HeaderText="kiekis" SortExpression="kiekis" />
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
        <br />
        <asp:Label ID="IvedDuomLabel" runat="server" Text="Įveskite duomenis reikalingus formuoti prenumeruotojų sąrašą:"></asp:Label>
        <br />
        <asp:TextBox ID="LeidinioPavTextBox" runat="server">Leidinio pavadinimas</asp:TextBox>
        <asp:TextBox ID="LeidinioMenNrTextBox" runat="server" Width="235px">Prenumeratos mėnesio numeris</asp:TextBox>
        <br />
        <br />
        <asp:Label ID="LeidFailPasirinkLabel" runat="server" Text="Pasirinkite leidinių pradinį duomenų failą:"></asp:Label>
        <br />
        <asp:FileUpload ID="LeidFileUpload" runat="server" />
        <br />
        <br />
        <asp:Label ID="PrenumFailPasirinkLabel" runat="server" Text="Pasirinkite prenumeratorių pradinį duomenų failą:"></asp:Label>
        <br />
        <asp:FileUpload ID="PrenumFileUpload" runat="server" />
        <br />
        <br />
        <asp:Button ID="VykdymoButton" runat="server" OnClick="Button1_Click" Text="Vykdyti!" Width="120px" />
        <br />
        <br />
    </div>
    <div class ="rezultatai">

        <asp:Label ID="MenesiuSarasoLabel" runat="server" Text="Daugiausiai pajamų generuojančių leidinių pavadinimų sąrašas, pagal mėnesius:"></asp:Label>
        <asp:Table ID="MenesiuTable" runat="server" BorderStyle="Solid" style="height: 28px">
        </asp:Table>
        <br />
        <asp:Label ID="BLeidPajLabel" runat="server" Text="Bendrosios leidinių pajamos:"></asp:Label>
        <asp:TextBox ID="BLeidiniuPajTextBox" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="LeidMazUzVidLabel" runat="server" Text="Leidinių, kurių pajamos mažesnės už vidutines sąrašas:"></asp:Label>
        <asp:Table ID="LeidMazUzVidTable" runat="server" BorderStyle="Solid">
        </asp:Table>
        <br />
        <asp:Label ID="PrenumSarPagalMenIrPavLabel" runat="server" Text="Prenumeratorių sąrašas, pagal nurodytą pavadinimą ir mėnesio numerį:"></asp:Label>
        <asp:Table ID="PrenumSarPagalMenIrPavTable" runat="server" BorderStyle="Solid">
        </asp:Table>

    </div>
    </form>
</body>
</html>
