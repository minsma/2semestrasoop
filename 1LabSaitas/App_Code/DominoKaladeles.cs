﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Domino kaladeliu konteineris
/// </summary>
public class DominoKaladeles
{
    const int MaxKadadeliuKiekis = 7; // Maksimalus domino kaladeliu kiekis sekoje
    
    private DominoKaladele[] dominoKaladeles; // Domino kaladeliu masyvas
    public int Kiekis { get; private set; } // Domino kaladeliu kiekis

    /// <summary>
    /// Konstruktorius, sukuriantis pradini domino kaladeliu masyva, MaxKaladeliuKiekis dydzio, taip pat, prisikiariama nuline reiksme kiekio kintamajam
    /// </summary>
    public DominoKaladeles()
    {
        dominoKaladeles = new DominoKaladele[MaxKadadeliuKiekis];
        Kiekis = 0;
    }
    /// <summary>
    /// Funkcija pridedanti kaladele i masyva
    /// </summary>
    /// <param name="dominoKaladele">Pridedamoji kaladele</param>
    public void PridetiKaladele(DominoKaladele dominoKaladele)
    {
        dominoKaladeles[Kiekis++] = dominoKaladele;
    }
    /// <summary>
    /// Funkcija pridedanti kaladele i nurodyta vieta
    /// </summary>
    /// <param name="dominoKaladele">Pridedama domino kaladele</param>
    /// <param name="vieta">Vieta i kuria norima prideti domino kaladele</param>
    public void PridetiKaladeleIVieta(DominoKaladele dominoKaladele, int vieta)
    {
        Kiekis++; // padidanamas masyvo dydis

        for (int i = Kiekis; i > vieta; i--)
        {
            dominoKaladeles[i] = dominoKaladeles[i - 1]; // perstumiamos reiksmes per viena aukstyn
        }

        dominoKaladeles[vieta] = dominoKaladele; //  idedama dominoKaladele reiksme
    }
    /// <summary>
    /// Funkcija grazinanti domino kaladele is konteinerio, pagal nurodyta indeksa
    /// </summary>
    /// <param name="indeksas">Indeksas pagal kuri bus grazinama kaladele is konteinerio</param>
    /// <returns>Kaladele, kurios indeksas sutampa su ieskomojo</returns>
    public DominoKaladele PaimtiKaladele(int indeksas)
    {
        return dominoKaladeles[indeksas];
    }
    /// <summary>
    /// Funkcija pasalinanti kaladele pagal nurodyta vieta
    /// </summary>
    /// <param name="vieta">Salinamosios kaladeles vieta konteinerio masyve</param>
    public void PasalintiKaladele(int vieta)
    {
        for (int i = vieta; i < Kiekis - 1; i++)
        {
            dominoKaladeles[i] = dominoKaladeles[i + 1];
        }
        Kiekis--;
    }
}