﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Domino kaladeles klase
/// </summary>
public class DominoKaladele
{
    private string skaiciuPora; // Saugomi skaiciai poromis nurodantys skaicius esancius dviejose domino kaladeles pusese

    /// <summary>
    /// Konstruktorius
    /// </summary>
    /// <param name="skaiciuPora">Idedama skaiciu pora i domino klase</param>
    public DominoKaladele(string skaiciuPora)
    {
        this.skaiciuPora = skaiciuPora;
    }
    /// <summary>
    /// Funckija paimanti domino kaladeles skaiciu pora
    /// </summary>
    /// <returns>Grazinama kaladeles skaiciu pora</returns>
    public string PaimtiKaladelesSkaiciuPora()
    {
        return skaiciuPora;
    }
    /// <summary>
    /// Funkcija apvercianti skaiciu, kitaip tariant apkeicianti skaicius vietomis
    /// </summary>
    public void ApverstiSkaiciu()
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(skaiciuPora[1]);
        stringBuilder.Append(skaiciuPora[0]);
        skaiciuPora = stringBuilder.ToString();
    }
}