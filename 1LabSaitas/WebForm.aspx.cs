﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebForm : System.Web.UI.Page
{
    public const int MaxKaladeliuKiekis = 7; // maksimalus kaladeliu kiekis
    const int MaxSekuKiekis = 100; // maksimalus leistinas seku kiekis

    const string CDfv = "Kur3.txt"; // pradiniu duomenu failo pavadinimas
    const string CDfr = "Ats.txt"; // rezultatu failo pavadinimas
    const string PradDuom = "PradiniaiDuomenys.txt"; // nuskaitytu pradiniu duomenu spausdinimui skirto failo pavadinimas

    /// <summary>
    /// Sukuriama lentele ir jos pavadinimas
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        TableRow row = new TableRow();
        TableCell pavadinimas = new TableCell(); // lenteles pavadinimas
        pavadinimas.Text = "<b>Seka</b>";
        row.Cells.Add(pavadinimas);
        Table1.Rows.Add(row);
    }
    /// <summary>
    /// Po mygtuko paspaudimo vykdomi skaiciavimai ir isvedami rezultatai
    /// </summary>
    protected void Button1_Click(object sender, EventArgs e)
    {
        DominoKaladeles dominoKaladeles; // domino kaladeliu konteineris

        Skaitymas(out dominoKaladeles, CDfv);
        SpausdinimasPradiniuDuomenu(dominoKaladeles, PradDuom);

        string[] sekos = new string[MaxSekuKiekis]; // sekoms saugoti skirtas masyvas
        int sekuKiekis; // seku kiekis
        
        SekuSudarymas(dominoKaladeles, sekos, out sekuKiekis);

        SpausdinimasSeku(sekos, sekuKiekis, CDfr);

        SpaudindimasVartotojoSasajai(sekos, sekuKiekis);
    }
    /// <summary>
    /// Funkcija skirta skaityti pradinius duomenis is tekstinio failo pavadinimu fv i domino kaladeliu konteineri
    /// </summary>
    /// <param name="dominoKaladeles">Domino kaladeliu konteineris</param>
    /// <param name="fv">Pradiniu duomenu tekstinio failo pavadinimas</param>
    protected void Skaitymas(out DominoKaladeles dominoKaladeles, string fv)
    {
        dominoKaladeles = new DominoKaladeles();

        string[] visosEilutes = File.ReadAllLines(Server.MapPath("App_Data/" + fv));

        string[] skaiciaiPoromis = visosEilutes[0].Split(' ');

        foreach (string skaiciusPora in skaiciaiPoromis)
        {
            DominoKaladele dominoKaladele = new DominoKaladele(skaiciusPora);
            dominoKaladeles.PridetiKaladele(dominoKaladele);
        }
    
    }
    /// <summary>
    /// Funckija, kurioje pradedamos formuoti sekos, funkcija paima kiekviena skaiciu kaip pradini, tiek tiesiogini, koks jis yra, tiek apversta ir perduoda tolimesniam sekos kurimui.
    /// </summary>
    /// <param name="dominoKaladeles">Domino kaladeliu konteineris</param>
    /// <param name="sekos">Sekoms saugoti skirtas masyvas</param>
    /// <param name="sekuKiekis">Seku kiekio sekimui skirtas integer tipo kintamasis</param>
    protected void SekuSudarymas(DominoKaladeles dominoKaladeles, string[] sekos, out int sekuKiekis)
    {
        sekuKiekis = 0;
        DominoKaladeles panaudoti = new DominoKaladeles();

        for (int i = 0; i < dominoKaladeles.Kiekis; i++)
        {
            SekuFormavimas(ref dominoKaladeles, panaudoti, i, dominoKaladeles.PaimtiKaladele(i).PaimtiKaladelesSkaiciuPora(), dominoKaladeles.PaimtiKaladele(i), sekos, ref sekuKiekis);
            SekuFormavimas(ref dominoKaladeles, panaudoti, i, SukeistiSkaiciusVietomis(dominoKaladeles.PaimtiKaladele(i).PaimtiKaladelesSkaiciuPora()), dominoKaladeles.PaimtiKaladele(i), sekos, ref sekuKiekis);
        }
    }
    /// <summary>
    /// Seku formavimui skirta funkcija, turinti pradini sekos nari, ji iesko antro, jeigu randa rekursijos budu eina gilyn ieskodama trecio ir t.t, 
    /// jeigu neranda arba jau yra pasibaigusi gryzta atgal i SekuSudarymas funkcija ir ima kita pradini kintamaji
    /// </summary>
    /// <param name="dominoKaladeles">Domino kaladeliu konteineris, kuriame yra dar nepanaudoti domino</param>
    /// <param name="panaudoti">Domino kaladeliu konteineris, kuriame yra jau nepanaudoti domino</param>
    /// <param name="indeksas">Siame kintamajame fiksuojama ieskomos domino kaladeles vieta sekoje</param>
    /// <param name="seka">Seka sudurinejama siame kintamajame</param>
    /// <param name="dominoKaladele">Peruodama domino kaladele, kuri yra naudojama tuo metu</param>
    /// <param name="sekos">Seku masyvas, kuriame surenkamos visos rastos sekos</param>
    /// <param name="sekuKiekis">Surastu seku kiekis</param>
    protected void SekuFormavimas(ref DominoKaladeles dominoKaladeles, DominoKaladeles panaudoti, int indeksas, string seka, DominoKaladele dominoKaladele, string[] sekos, ref int sekuKiekis)
    {
        panaudoti.PridetiKaladele(dominoKaladeles.PaimtiKaladele(indeksas));
        dominoKaladeles.PasalintiKaladele(indeksas);

        for (int i = 0; i < dominoKaladeles.Kiekis; i++)
        { 
            // Jeigu sekos paskutinis skaiciu lygus kitos kaladeles pirmam skaiciui, tai skaitosi kaip rasta ieskoma kaladele ir pradedama ieskoti kitos sekai reikalingos domino kaladeles
            if (seka[seka.Length - 1] == dominoKaladeles.PaimtiKaladele(i).PaimtiKaladelesSkaiciuPora()[0])
            {
                SekuFormavimas(ref dominoKaladeles, panaudoti, i, seka + dominoKaladeles.PaimtiKaladele(i).PaimtiKaladelesSkaiciuPora(), dominoKaladeles.PaimtiKaladele(i), sekos, ref sekuKiekis);
            }
            // Jeigu sekos paskutinis skaiciu lygus kitos kaladeles antram skaiciui, tai domino kaladeles skaiciai sukeiciami vietomis ir tai skaitosi kaip rasta ieskoma kaladele
            //ir pradedama ieskoti kitos sekai reikalingos domino kaladeles
            else if (seka[seka.Length - 1] == dominoKaladeles.PaimtiKaladele(i).PaimtiKaladelesSkaiciuPora()[1])
            {
                SekuFormavimas(ref dominoKaladeles, panaudoti, i, seka + SukeistiSkaiciusVietomis(dominoKaladeles.PaimtiKaladele(i).PaimtiKaladelesSkaiciuPora()), dominoKaladeles.PaimtiKaladele(i), sekos, ref sekuKiekis);
            }
        }
        if (dominoKaladeles.Kiekis == 0) // jeigu domino kaladeliu kiekis baigiasi laikoma kaip rasta seka
        {
            sekos[sekuKiekis++] = seka;
        }

        dominoKaladeles = IterptiKaladeles(indeksas, dominoKaladeles, dominoKaladele);
        panaudoti.PasalintiKaladele(panaudoti.Kiekis - 1);
        seka = seka.Remove(seka.Length - 2);
    }
    /// <summary>
    /// Funkcija sukeicianti domino kaladeles skaicius vietomis
    /// </summary>
    /// <param name="skaiciuPora">Skaiciai pries apkeitima vietomis</param>
    /// <returns>Skaiciai po apkeitimo vietomis</returns>
    protected string SukeistiSkaiciusVietomis(string skaiciuPora)
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(skaiciuPora[1]);
        stringBuilder.Append(skaiciuPora[0]);
        return stringBuilder.ToString();
    }
    /// <summary>
    /// Iterpia domino kaladele i nurodyta vieta domino kaladeliu konteineryje
    /// </summary>
    /// <param name="vieta">Vieta, i kuria norima iterpti domino kaladele</param>
    /// <param name="dominoKaladeles">Domino kaladeliu konteineris</param>
    /// <param name="dominoKaladele">Iterpiama domino kaladele</param>
    /// <returns>Grazinamas domino konteineris po iterpimo</returns>
    protected DominoKaladeles IterptiKaladeles(int vieta, DominoKaladeles dominoKaladeles, DominoKaladele dominoKaladele)
    {
        DominoKaladeles dominoKaladelesNaujas = new DominoKaladeles();

        for (int i = 0; i < vieta; i++)
        {
            dominoKaladelesNaujas.PridetiKaladele(dominoKaladeles.PaimtiKaladele(i));
        }
        dominoKaladelesNaujas.PridetiKaladele(dominoKaladele);

        for (int i = vieta; i < dominoKaladeles.Kiekis; i++)
        {
            dominoKaladelesNaujas.PridetiKaladele(dominoKaladeles.PaimtiKaladele(i));
        }


        return dominoKaladelesNaujas;
    }
    /// <summary>
    /// Funckija spausdinanti sekas i tekstini faila
    /// </summary>
    /// <param name="sekos">Rastu seku masyvas</param>
    /// <param name="sekuKiekis">Seku kiekis</param>
    /// <param name="fr">Rezultatu failo pavadinimas fr</param>
    protected void SpausdinimasSeku(string[] sekos, int sekuKiekis, string fr)
    {
        using (StreamWriter writer = new StreamWriter(Server.MapPath("App_Data/" + fr)))
        {
            for (int i = 0; i < sekuKiekis; i++)
            {
                writer.WriteLine(new String('-', 29));
                writer.Write("| ");
                for (int j = 0; j < sekos[i].Length; j += 2)
                {
                    writer.Write("{0}{1}| ", sekos[i][j], sekos[i][j + 1]);
                }
                writer.WriteLine();
            }
            writer.WriteLine(new String('-', 29));
        }
    }
    /// <summary>
    /// Funkcija spausdinanti nuskaitytus pradinius duomenis i tekstini faila pavadinimu fr
    /// </summary>
    /// <param name="dominoKaladeles">Domino kaladeliu konteineris</param>
    /// <param name="fr">Failo pavadinimas i kuri bus spausdinama</param>
    protected void SpausdinimasPradiniuDuomenu(DominoKaladeles dominoKaladeles, string fr)
    {
        using (StreamWriter writer = new StreamWriter(Server.MapPath("App_Data/" + fr)))
        {
            writer.WriteLine(new String('-', 29));
            writer.Write("|");

            for (int i = 0; i < dominoKaladeles.Kiekis; i++)
            {
                writer.Write(dominoKaladeles.PaimtiKaladele(i).PaimtiKaladelesSkaiciuPora() + " |");
            }

            writer.WriteLine();
            writer.WriteLine(new String('-', 29));
        }
    }
    /// <summary>
    /// Funkcija spausdinanti sekas lentele vartotojo sasajoje
    /// </summary>
    /// <param name="sekos">Rastu seku masyvas</param>
    /// <param name="sekuKiekis">Seku kiekis</param>
    protected void SpaudindimasVartotojoSasajai(string[] sekos, int sekuKiekis)
    {
        for (int i = 0; i < sekuKiekis; i++)
        {
            TableRow row = new TableRow();
            int kiekis = 0;
            TableCell[] skaiciuPoros = new TableCell[MaxSekuKiekis];
            for (int j = 0; j < sekos[i].Length; j += 2)
            {
                skaiciuPoros[kiekis] = new TableCell();
                skaiciuPoros[kiekis].Text += sekos[i][j];
                skaiciuPoros[kiekis].Text += sekos[i][j + 1];
                kiekis++;
            }
            for(int j = 0; j < kiekis; j++)
            {
                row.Cells.Add(skaiciuPoros[j]);
            }

            Table1.Rows.Add(row);
        }
    }
}